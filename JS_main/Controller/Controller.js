function layThongTinTuForm() {
  var tk = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var pass = document.getElementById("password").value;
  var date = document.getElementById("datepicker").value;
  var chucVu = document.getElementById("chucvu").value;
  var luong = document.getElementById("luongCB").value * 1;
  var time = document.getElementById("gioLam").value * 1;
  var thongTinNV = new NhanVien(
    tk,
    name,
    email,
    pass,
    date,
    luong,
    chucVu,
    time
  );

  return thongTinNV;
}

function renderNV(nv) {
  var contentHTML = "";
  for (var i = 0; i < nv.length; i++) {
    var in4NV = nv[i];
    var content = `
        <tr>
        <td>${in4NV.tk}</td>
        <td>${in4NV.ten}</td>
        <td>${in4NV.email}</td>
        <td>${in4NV.date}</td>
        <td>${in4NV.chucVu}</td>
        <td>${in4NV.luongTong()}</td>
        <td>${in4NV.xepHang()}</td>
        <td><button onclick = "xoaNV(${
          in4NV.tk
        })" class ="btn btn-danger" >Xóa</button>
        <button class="btn btn-primary" onclick="capNhatSV()">Cập Nhật</button></td>
        
        </tr>
        `;
    contentHTML = contentHTML + content;
  }

  document.getElementById("tableDanhSach").innerHTML = contentHTML;
  return contentHTML;
}
function showThongTinLenForm(item) {
  document.getElementById("tknv").value = item.tk;
  document.getElementById("name").value = item.name;
  document.getElementById("email").value = item.email;
  document.getElementById("password").value = item.pass;
  document.getElementById("datepicker").value = item.date;
  document.getElementById("chucvu").value = item.chucVu;
  document.getAnimations("luongCB").value = item.luong;
  document.getElementById("gioLam").value = item.time;
}



