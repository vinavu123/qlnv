function NhanVien(_tk,_ten,_email,_pass,_date,_luong,_chucVu,_time){
    this.tk = _tk;
    this.ten = _ten;
    this.email = _email;
    this.pass = _pass;
    this.date = _date;
    this.luong = _luong;
    this.chucVu = _chucVu ;
    this.time = _time;
    this.luongTong = function(){
        var tongLuong = 0  
        if(this.chucVu == "Nhân Viên"){
          tongLuong = this.luong
        }
        else if (this.chucVu =="Trưởng Phòng"){
            tongLuong = this.luong*2
        }
        else if (this.chucVu =="Sếp"){
           tongLuong = this.luong*3
        }
        return tongLuong ; 
    };
    
    this.xepHang = function(){
        var loai = ""
        if(this.time<160){
            loai = "Nhân Viên Trung Bình"
        }
        else if(this.time<176){
            loai = "Nhân Viên Khá"
        } 
        else if(this.time<192){
            loai ="Nhân Viên giỏi"
        }
        else{
            loai = "Nhân Viên Xuất Sắc"
        }
        return loai ; 
    };

}