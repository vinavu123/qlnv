var listNV = [];
var dataJson = localStorage.getItem("DATA_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var nv = new NhanVien(
      item.tk,
      item.ten,
      item.email,
      item.pass,
      item.date,
      item.luong,
      item.chucVu,
      item.time
    );
    listNV.push(nv);
  }
  renderNV(listNV);
}
function themNV() {
  var thongTinForm = layThongTinTuForm();
  // kiem tra Tai khoan
  var isValid =kiemTraRong("tbTKNV", thongTinForm.tk)&&
    kiemTraTK("tbTKNV", thongTinForm.tk) 
    
  //  kiem tra ten
  isValid = isValid & kiemTraRong("tbTen", thongTinForm.ten);
  //  kiem ta email
  isValid =
    isValid & kiemTraRong("tbEmail", thongTinForm.email) && kiemTraEmail(thongTinForm.email) 
    
  //  kiem tra pass
  isValid =
    isValid & kiemTraRong("tbMatKhau",thongTinForm.pass) && kiemTraPass(thongTinForm.pass) 
    
  // kiem tra ngay
  isValid = isValid & kiemTraRong("tbNgay", thongTinForm.date);
  // kiem tra luong
  isValid =
    isValid & kiemTraRong("tbLuongCB", thongTinForm.luong)&& kiemTraLuong("tbLuongCB", thongTinForm.luong) 
    
  //  kiem tra chuc vu
  isValid =
    isValid & kiemTraRong("tbChucVu", thongTinForm.chucVu)&& kiemTraChucVu("tbChucVu", thongTinForm.chucVu) 
    
  // kiem tra gio lam
  isValid =
    isValid & kiemTraRong("tbGioLam", thongTinForm.time) && kiemTranTime("tbGioLam", thongTinForm.time) 
    
  if (isValid == true) {
    listNV.push(thongTinForm);
    var dataJson = JSON.stringify(listNV);
    localStorage.setItem("DATA_LOCAL",dataJson)
    renderNV(listNV);
  }
}
function xoaNV(nvArr) {
  var viTri = -1;
  for (var i = 0; i < listNV.length; i++) {
    var nv = listNV[i];
    if (nvArr == nv.tk) {
      viTri = i;
    }
  }
  listNV.splice(viTri, 1);
  renderNV(listNV);
}
function capNhatSV(id){
  var viTri = listNV.findIndex(function (item) {

    return item.tk == id;
  });
  
  if (viTri != -1) {
    document.getElementById("tknv").disabled = true;
    showThongTinLenForm(listNV[viTri]);
  }
}
function capNhat(){
   document.getElementById("tknv").disabled = false;

var nv = layThongTinTuFrom();
var viTri = listNV.findIndex(function (item) {
  return item.tk == nv.tk;
});
if (viTri !== -1) {
  listNV[viTri] = nv;
  renderNV(listNV)
 
}}
(function timNV(){
  
  var name = document.getElementById("searchName").value
  for(var i = 0; i<listNV.length;i++){
var nV = listNV[i]
var viTri = findIndex(function(item){
  item.chucVu = nV.chucVu
})
listNV[viTri] = nV
renderNV(listNV)


  }
})
