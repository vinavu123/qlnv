var showMessage = function (id,mess){
  document.getElementById(id).innerHTML = mess
};
var kiemTraRong = function (idErr, value) {
  if (value.length == 0) {
    showMessage(idErr, "Bạn Chưa Nhập Nội Dung");
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};
var kiemTraTK = function (tkErr, value) {
  if (value.length >= 4 && value.length <= 6) {
    showMessage(tkErr, "");
    return true;
  } else {
    showMessage(tkErr, "Tài Khoản từ 4 đến 6 ký số");
    return false;
  }
};
var kiemTraLuong = function (luongErr, value) {
  if (value >= 1000000 && value <= 20000000) {
    showMessage(luongErr, "");
    return true;
  } else {
    showMessage(luongErr, "Nhập lương từ 1000000-20000000");
    return false;
  }
};
var kiemTraChucVu = function (cvErr, value) {
  if (value == "Chọn chức vụ") {
    showMessage(cvErr, "Vui lòng chọn chức vụ");
    return false;
  } else showMessage(cvErr, "");
  return true;
};
var kiemTranTime = function (timeErr, value) {
  if (value >= 80 && value <= 200) {
    showMessage(timeErr, "");
  } else {
    showMessage(timeErr, "Giờ làm từ 80-200 giờ");
    return false;
  }
};
var kiemTraEmail = function (email) {
  const re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (re.test(email)) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email không hợp lệ");
    return false;
  }
};
var kiemTraPass = function (pass) {
  const re = /^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,10}$/;
  if (re.test(pass)) {
    showMessage("tbMatKhau", "");
    return true;
  } else {
    showMessage("tbMatKhau", "Pass không hợp lệ");
    return false;
  }
};
